# Include replacer

- Replaces `<include path="path/to/file.txt">` with the contents of `path/to/file.txt`
    - Input files must be text (UTF-8 encoding)
    - Included files can be binary

## Downloads

- [Browse pre-built binaries](https://gitlab.com/JonstonChan/include-replacer/-/jobs/artifacts/main/browse?job=build)
  - You may need to enable the executable bit with `chmod +x include-replacer`
    or equivalent, depending on your operating system


## Use cases

### Client-only HTML document

A [reveal.js presentation][reveal.js presentation - source] ([live demo][reveal.js presentation - live demo])
has each `<section>` as a slide.

`include-replacer` can reduce the clutter:

```html
<div class="slides">
  <include path="slides/intro.html">
  <include path="slides/keyboard-shortcuts.html">
</div>
```

where each `<include>` contains:

```html
<section>
  <!-- slide contents -->
</section>
```

### [GitLab file-type CI/CD variables](https://web.archive.org/web/20230216111847/https://docs.gitlab.com/ee/ci/variables/#use-file-type-cicd-variables)

Instead of using `cat before.txt secret.txt after.txt`
or search-and-replace, an `<include>` can be used.


### Cases where LaTeX `input` does not work

The LaTeX `\input{filepath}` macro usually inserts file contents
at that position as if a user has typed it in.
There are [cases where it does not work](https://tex.stackexchange.com/a/653657)
and an alternative has to be used.

More generally, any data that is generated from a different part
of the pipeline can be included.


## Examples

A comprehensive list of examples can be found in the `examples` directory

### Basic includes

```console
> ./include-replacer < input.txt
A single line include:
one-line include file

A multi-line include:
multi-line include file: line 1
multi-line include file: line 2
multi-line include file: line 3

Some more text after
```

> **`input.txt`**:
>
> ```txt
> A single line include:
> <include path="includes/single-line.txt">
>
> A multi-line include:
> <include path="includes/multi-line.txt">
>
> Some more text after
> ```

> **`includes/single-line.txt`**:
>
> ```txt
> one-line include file
> ```

> **`includes/multi-line.txt`**:
>
> ```txt
> multi-line include file: line 1
> multi-line include file: line 2
> multi-line include file: line 3
> ```


### Options

#### Preserve leading spaces

```console
> ./include-replacer < input.txt
<div>
  <ul>
    <li>Item 1</li>
    <li>Item 2</li>
    <li>Item 3</li>
  </ul>
</div>
```

> **`input.txt`**:
>
> ```html
> <div>
>  <include path="includes/list.html" preserve-leading-spaces=true>
> </div>
> ```

> **`includes/list.html`**:
>
> ```html
> <ul>
>   <li>Item 1</li>
>   <li>Item 2</li>
>   <li>Item 3</li>
> </ul>
> ```


### Additional notes

For readability, options can be separated with whitespace:

```html
<include
	path="includes/include.txt">

<include
	path="includes/include.txt"
>
```

If not a syntactically-correct `<include>`,
then assumes it isn't an include statement
and simply echos it out:

```html
<include
	path="includes/file-a.txt"
	unknown-flag>
```

The last of any repeated option is used:

```html
<include
	path="includes/file-a.txt"
	path="includes/file-b.txt">
```

Upon inclusion of an unreadable file, displays an error and exits with code `1`:

```html
<!-- 2023/01/01 00:00:00 open non-existent.txt: no such file or directory -->
<include path="non-existent.txt">
```


[reveal.js presentation - live demo]: https://technology-tests.gitlab.io/javascript/revealjs-showcase/
[reveal.js presentation - source]: https://gitlab.com/technology-tests/javascript/revealjs-showcase/-/blob/bc643c1c5b3a57e6848ea1e6ac723ec9fb14977f/public/index.html#L87-94
