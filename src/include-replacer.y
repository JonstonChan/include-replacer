%{
package main

import (
	"fmt"
	"log"
	"os"
)
%}

%union {
  optionPreserveLeadingSpaces bool
  leadingSpaces string
  path string
  text string
  returnType int
}

%token INCLUDE
%token TEXT

%%
input:     /* empty */
         | input segment
;

segment:   TEXT     { fmt.Print($1.text); }
         | INCLUDE  {
                       filepath := $1.path

                       // Lexer "eats up" the leading spaces,
                       // so output it again
                       fmt.Print($1.leadingSpaces)

                       data, err := os.ReadFile(filepath)
                       if err != nil {
                           log.Fatal(err)
                       }

                       if !$1.optionPreserveLeadingSpaces {
                           fmt.Print(string(data))
                       } else {
                           for _, v := range data {
                               fmt.Print(string(v))

                               if v == '\n' {
                                   fmt.Print($1.leadingSpaces)
                               }
                           }
                       }
                    }
;
%%
