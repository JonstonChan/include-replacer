#!/usr/bin/env bash

set -e

executable="$(realpath include-replacer)"

for example_dir in examples/*/; do
    (
        cd "$example_dir"
        printf "Testing %s\n" "$example_dir"

        "$executable" < input.txt > output-actual.txt

        diff output.txt output-actual.txt
    )
done

echo "All tests passed"
